" My little .vimrc with Plug.vim
" from nac

set mouse=a
set encoding=utf-8
set number
set autoindent
set hlsearch
set showcmd
setf dosini
set autoread
set tabstop=2
set cursorcolumn
set cursorline
set wrap
set linebreak

set nospell
autocmd FileType markdown setlocal spell
setlocal spell spelllang=de_de,en_us

if has("autocmd")
  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
endif

" Install vim-plug if not found
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
endif

" Run PlugInstall if there are missing plugins
autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \| PlugInstall --sync | source $MYVIMRC
\| endif

" plug.vim
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')

" automatic closing of quote, parenthesis, brackets, etc.,
Plug 'Raimondi/delimitMate'

" Syntastic is a syntax checking plugin
Plug 'scrooloose/syntastic'
	set statusline+=%#warningmsg#
	set statusline+=%{SyntasticStatuslineFlag()}
	set statusline+=%*
	let g:syntastic_always_populate_loc_list = 1
	let g:syntastic_auto_loc_list = 1
	let g:syntastic_check_on_open = 1
	let g:syntastic_check_on_wq = 0

" Supertab is a vim plugin which allows you to us <TAB> for all your insert
" completion need (:help ins-completion).
Plug 'ervandew/supertab'

" Statusline
Plug 'vim-airline/vim-airline'

" Makrdown Preview
Plug 'instant-markdown/vim-instant-markdown', {'for': 'markdown'}
	filetype plugin on
	"Uncomment to override defaults:
	let g:instant_markdown_slow = 0
	let g:instant_markdown_autostart = 0
	let g:instant_markdown_allow_unsafe_content = 0
	let g:instant_markdown_allow_external_content = 1
	let g:instant_markdown_mathjax = 1
	let g:instant_markdown_mermaid = 1
	let g:instant_markdown_autoscroll = 0
	let g:instant_markdown_browser = "firefox --new-window"

" Lua syntax highlighting
Plug 'tbastos/vim-lua'
let g:lua_syntax_someoption = 1

" Log syntax highlighting
Plug 'mtdl9/vim-log-highlighting'

" toml syntax
Plug 'cespare/vim-toml', { 'branch': 'main' }

" yaml syntax
Plug 'stephpy/vim-yaml'

" vim-markdown
Plug 'gabrielelana/vim-markdown'

" nginx
Plug 'chr4/nginx.vim'

" lervag/vimtex
Plug 'lervag/vimtex'
	filetype plugin indent on
	syntax enable
	let g:vimtex_view_method = 'zathura'
	let g:vimtex_view_general_viewer = 'evince'
	let g:vimtex_view_general_options = '--unique file:@pdf\#src:@line@tex'
	let g:vimtex_compiler_method = 'latexmk'
	let maplocalleader = ","

" Plug 'chriskempson/base16-vim'
Plug 'chriskempson/base16-vim'

" Plug 'dpelle/vim-LanguageTool'
Plug 'dpelle/vim-LanguageTool'

" Plug 'junegunn/limelight.vim'
Plug 'junegunn/limelight.vim'
	" Color name (:help cterm-colors) or ANSI code
	let g:limelight_conceal_ctermfg = 'gray'
	let g:limelight_conceal_ctermfg = 240

	" Default: 0.5
	let g:limelight_default_coefficient = 0.7

	" Number of preceding/following paragraphs to include (default: 0)
	let g:limelight_paragraph_span = 1

	" Beginning/end of paragraph
	"   When there's no empty line between the paragraphs
	"   and each paragraph starts with indentation
	"let g:limelight_bop = '^\s'
	"let g:limelight_eop = '\ze\n^\s'

	" Highlighting priority (default: 10)
	"   Set it to -1 not to overrule hlsearch
	let g:limelight_priority = -1


"----------------------------------------------------------------------------"
"--------------------- Mapping ----------------------------------------------"
"----------------------------------------------------------------------------"

map<F4> :InstantMarkdownPreview<CR>
map<F5> :InstantMarkdownStop<CR>
map<F9> :set nospell<CR>

call plug#end()
