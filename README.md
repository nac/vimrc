# My little .vimrc
I use [vim-plug](https://github.com/junegunn/vim-plug) for my Plugins

## Plugins
- [delimitMate](https://github.com/Raimondi/delimitMate)
- [syntastic](https://github.com/vim-syntastic/syntastic)
- [supertab](https://github.com/ervandew/supertab)
- [vim-airline](https://github.com/ervandew/supertab)
- [vim-instant-markdown](https://github.com/instant-markdown/vim-instant-markdown)
- [vim-lua](https://github.com/tbastos/vim-lua)
- [vim-log-ighlighting](https://github.com/MTDL9/vim-log-highlighting)
- [vim-toml](https://github.com/cespare/vim-toml)
- [base16-vim](https://github.com/chriskempson/base16-vim.git)
- [vim-markdown](https://github.com/gabrielelana/vim-markdown)

## vim-instant-markdown
see [Install Instructions](https://github.com/instant-markdown/vim-instant-markdown)

## Key Mapping
- `<F4>` start vim-instant-markdown
- `<F5>` stop vim-instant-markdown
- `<F9>` stop German Spell Check
